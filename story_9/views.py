from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def index(request):
	if not request.user.is_authenticated:
		return render(request, 'login.html', {})
	return render(request, 'index.html', {})

def login_view(request):
	if request.method == "POST":
		username = request.POST["username"]
		password = request.POST["password"]
		user = authenticate(request, username=username, password=password)
		response = {"user" : user}
		if user is not None:
			login(request, user)
			return render(request, 'index.html', response)
		else:
			return render(request, 'login.html', {"message": "Invalid username or password."})

	return render(request, 'login.html', {"message": None})

def logout_view(request):
	logout(request)
	return render(request, 'login.html', {})

def register(request):
	if request.method == "POST":
		first_name = request.POST["first_name"]
		last_name = request.POST["last_name"]
		username = request.POST["username"]
		email = request.POST["email"]
		password = request.POST["password"]
		new_user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username, email=email, password=password)
		new_user.save()

		user = authenticate(request, username=username, password=password)
		login(request, user)
		response = {"user" : user}
		return render(request, 'login.html', response)

	return render(request, "register.html")