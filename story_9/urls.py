from django.contrib import admin
from django.urls import *
from .views import *

app_name='story_9'
urlpatterns = [
    path('', index, name='index'),
    path('login/', login_view, name='login'),
	path('logout/', logout_view, name='logout'),
	path('register/', register, name='register'),
]