from django.test import TestCase
from django.urls import resolve
from .views import index, login_view, logout_view, register
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

class Story9UnitTest(TestCase):
	def setUp(self):
		first_name = 'Louise'
		last_name = 'Banks'
		username = 'louise.banks'
		email = 'louisebanks@arrival.com'
		password = 'linguisticsexpert'
		new_user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username, email=email, password=password)
		new_user.save()

	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)
	
	def test_url_index_if_authenticated(self):
		self.client.login(username='louise.banks', password='linguisticsexpert')
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_template_if_authenticated(self):
		self.client.login(username='louise.banks', password='linguisticsexpert')
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_context_message_if_authenticated(self):
		self.client.login(username='louise.banks', password='linguisticsexpert')
		response = self.client.get('/')
		user = response.context['user']
		self.assertIsInstance(user, User)

	def test_url_index_if_not_authenticated(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_login_template_if_not_authenticated(self):
		response = self.client.get('/', follow=True)
		self.assertTemplateUsed(response, 'login.html')

	def test_using_login_view_function(self):
		found = resolve('/login/')
		self.assertEqual(found.func, login_view)

	def test_url_login_if_get_method(self):
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_using_login_template_if_get_method(self):
		response = self.client.get("/login/")
		self.assertTemplateUsed(response, 'login.html')

	def test_context_message_if_get_method(self):
		response = self.client.get('/login/')
		message = response.context['message']
		self.assertEqual(message, None)

	def test_url_login_if_post_method_and_auth_failed(self):
		response = self.client.post('/login/', data={'username': 'ian.donnelly', 'password': 'theoreticalphysicist'})
		self.assertEqual(response.status_code, 200)

	def test_using_login_template_if_post_method_and_auth_failed(self):
		response = self.client.post('/login/', data={'username': 'ian.donnelly', 'password': 'theoreticalphysicist'})
		self.assertTemplateUsed(response, 'login.html')